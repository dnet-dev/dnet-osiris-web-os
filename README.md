<h1>Osiris Web OS</h1>
Osiris Web Operating System will be design with considerations in process management, protection, I/O management, memory management, decentralized file and storage management.
 
<h2>Process Management</h2>
This OS will efficiently handles processes such as installed extensions and plugins in Osiris browser.
Tasks of Process Management of an OS:
●       It creates, loads, executes, suspends, resumes, and terminates processes
●       Will have process scheduling where it switch system among multiple processes in main memory
●       Provides communication mechanisms so that processes can send or receive data to and from each other
●       Process synchronization to concurrently control access to shared data to keep shared data consistent
●       Allocates or deallocates resources properly to prevent or avoid deadlock situation
In time-shared systems, even if the system is equipped with a single CPU, we usually say that the processes in main memory are concurrently running.
Deadlock situation: e.g., There are three running processes A, B, and C. A is waiting for the resource of B. B is waiting for the resource of C. C is waiting for the resource of A. At the same time they want to keep using their current resources. As a result, all processes will wait forever

<h2>Protection</h2>
Protect hardware resources, kernel codes, processes, files, and data from erroneous programs and malicious programs.

<h2>Input and Output Management</h2>
Decentralized real time media will be manage by Cronus.
Keep the details from applications to ensure proper use of devices, to prevent errors, and to provide users with convenient and efficient programming environment.
Tasks of I/O Management of OS: 
Hide the details of H/W devices
Manage main memory for the devices using cache, buffer, and spooling 
Maintain and provide device driver interfaces

<h2>Memory management</h2>
 
<h2>File & Storage Management</h2>

